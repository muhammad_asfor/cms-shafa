import 'package:progiom_cms/src/features/core/core.dart';
import 'package:progiom_cms/src/features/core/util/util.dart';
import 'package:dartz/dartz.dart';
import 'package:progiom_cms/src/features/notifications/domain/repositories/NotificationsRepository.dart';

class DisableEnableNotification
    extends UseCase<bool, DisableEnableNotificationParams> {
  final NotificationsRepository repository;
  DisableEnableNotification(this.repository);

  @override
  Future<Either<Failure, bool>> call(
      DisableEnableNotificationParams params) async {
    return await repository.disableEnableNotification(enable: params.enable);
  }
}

class DisableEnableNotificationParams {
  final bool enable;
  DisableEnableNotificationParams({required this.enable});
}

import 'package:progiom_cms/src/features/core/core.dart';
import 'package:progiom_cms/src/features/core/util/util.dart';
import 'package:dartz/dartz.dart';
import 'package:progiom_cms/src/features/notifications/domain/repositories/NotificationsRepository.dart';

class SetNotificationAsRead extends UseCase<bool, SetNotificationAsReadParams> {
  final NotificationsRepository repository;
  SetNotificationAsRead(this.repository);

  @override
  Future<Either<Failure, bool>> call(SetNotificationAsReadParams params) async {
    return await repository.setNotifiationAsRead(
      params.notificationId,
    );
  }
}

class SetNotificationAsReadParams {
  final String notificationId;
  SetNotificationAsReadParams({required this.notificationId});
}

import 'package:url_launcher/url_launcher.dart';

openWhatsapp(String phone) async {
  final Uri _url = Uri.parse('https://wa.me/$phone');

  await launchUrl(_url);
}

sendMessageToWhatsapp(String phone, String message) async {
  final Uri _url =
      Uri.parse("https://api.whatsapp.com/send?phone=$phone&text=$message");

  await launchUrl(_url);
}

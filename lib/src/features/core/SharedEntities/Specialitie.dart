import 'package:json_annotation/json_annotation.dart';
import 'package:progiom_cms/src/features/core/SharedEntities/dynamic_field.dart';
part 'Specialitie.g.dart';

@JsonSerializable(fieldRename: FieldRename.snake)
class Specialitie {
  final int id;
  final int? status;
  final int? sorting;
  final String coverImage;
  final String title;
  // Null parent;

  Specialitie({
    required this.id,
    this.status,
    this.sorting,
    required this.coverImage,
    required this.title,
  });

  factory Specialitie.fromJson(json) => _$SpecialitieFromJson(json);
  toJson() => _$SpecialitieToJson(this);
  static List<Specialitie> fromJsonList(List json) {
    return json.map((e) => Specialitie.fromJson(e)).toList();
  }
}

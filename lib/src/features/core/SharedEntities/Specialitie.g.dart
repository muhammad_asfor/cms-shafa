// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'Specialitie.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

Specialitie _$SpecialitieFromJson(Map<String, dynamic> json) => Specialitie(
      id: json['id'] as int,
      status: json['status'] as int?,
      sorting: json['sorting'] as int?,
      coverImage: json['cover_image'] as String,
      title: json['title'] as String,
    );

Map<String, dynamic> _$SpecialitieToJson(Specialitie instance) =>
    <String, dynamic>{
      'id': instance.id,
      'status': instance.status,
      'sorting': instance.sorting,
      'cover_image': instance.coverImage,
      'title': instance.title,
    };

import 'package:progiom_cms/src/features/core/core.dart';
import 'package:dartz/dartz.dart';
import 'package:progiom_cms/src/features/ecommerce/domain/repositories/Ecommerce_repository.dart';
import 'package:progiom_cms/src/features/homeSettings/domain/entities/TopDoctor.dart';

class GetProducatsByPoints
    extends UseCase<List<TopDoctor>, ProducatsByPointsParams> {
  final EcommerceRepository repository;
  GetProducatsByPoints(this.repository);

  @override
  Future<Either<Failure, List<TopDoctor>>> call(
      ProducatsByPointsParams params) async {
    return await repository.getListProducts(
      endpoint: "/posts" +
          "?points_price[min]=1"
              "&page=" +
          params.page.toString(),
    );
  }
}

class ProducatsByPointsParams {
  final int page;

  ProducatsByPointsParams({
    required this.page,
  });
}

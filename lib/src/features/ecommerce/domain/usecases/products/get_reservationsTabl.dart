import 'package:progiom_cms/src/features/core/core.dart';
import 'package:progiom_cms/src/features/core/util/util.dart';
import 'package:dartz/dartz.dart';
import 'package:progiom_cms/src/features/ecommerce/domain/repositories/Ecommerce_repository.dart';
import 'package:progiom_cms/src/features/homeSettings/domain/entities/TopDoctor.dart';
import 'package:progiom_cms/src/features/homeSettings/domain/entities/TopDoctorWithReviews.dart';

class GetReservationsTablUsecass
    extends UseCase<Map<dynamic, dynamic>, ReservationsTablParams> {
  final EcommerceRepository repository;
  GetReservationsTablUsecass(this.repository);

  @override
  Future<Either<Failure, Map<dynamic, dynamic>>> call(
      ReservationsTablParams params) async {
    return await repository.getReservationsTabl(
        id: params.id, date: params.date);
  }
}

class ReservationsTablParams {
  final String id;
  final String date;
  ReservationsTablParams({required this.id, required this.date});
}

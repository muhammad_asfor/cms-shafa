import 'package:progiom_cms/src/features/core/core.dart';
import 'package:progiom_cms/src/features/core/util/util.dart';
import 'package:dartz/dartz.dart';
import 'package:progiom_cms/src/features/ecommerce/domain/repositories/Ecommerce_repository.dart';

class AddReservationsUsecass extends UseCase<bool, AddReservationsParams> {
  final EcommerceRepository repository;
  AddReservationsUsecass(this.repository);

  @override
  Future<Either<Failure, bool>> call(AddReservationsParams params) async {
    return await repository.setReservations(params: params);
  }
}

class AddReservationsParams {
  final int doctor_id;
  final String date;
  final String time;
  AddReservationsParams(
      {required this.doctor_id, required this.time, required this.date});
}

import 'package:progiom_cms/src/features/core/core.dart';
import 'package:progiom_cms/src/features/core/util/util.dart';
import 'package:dartz/dartz.dart';
import 'package:progiom_cms/src/features/ecommerce/domain/repositories/Ecommerce_repository.dart';
import 'package:progiom_cms/src/features/homeSettings/domain/entities/TopDoctor.dart';
import 'package:progiom_cms/src/features/homeSettings/domain/entities/TopDoctorWithReviews.dart';

class GetProductDetails
    extends UseCase<TopDoctorWithReviews, GetProductDetailsParams> {
  final EcommerceRepository repository;
  GetProductDetails(this.repository);

  @override
  Future<Either<Failure, TopDoctorWithReviews>> call(
      GetProductDetailsParams params) async {
    return await repository.getProductDetails(id: params.id);
  }
}

class GetProductDetailsParams {
  final String id;
  GetProductDetailsParams({required this.id});
}

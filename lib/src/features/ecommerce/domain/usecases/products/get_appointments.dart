import 'package:progiom_cms/src/features/core/core.dart';
import 'package:progiom_cms/src/features/core/util/util.dart';
import 'package:dartz/dartz.dart';
import 'package:progiom_cms/src/features/ecommerce/domain/repositories/Ecommerce_repository.dart';
import 'package:progiom_cms/src/features/homeSettings/domain/entities/TopDoctor.dart';
import 'package:progiom_cms/src/features/homeSettings/domain/entities/TopDoctorWithReviews.dart';

class GetAppointmentsUsecass extends UseCase<Map<dynamic, dynamic>, String> {
  final EcommerceRepository repository;
  GetAppointmentsUsecass(this.repository);

  @override
  Future<Either<Failure, Map<dynamic, dynamic>>> call(String isOld) async {
    return await repository.getAppointments(isOld);
  }
}

// class ReservationsTablParams {
//   final String id;
//   final String date;
//   ReservationsTablParams({required this.id, required this.date});
// }

import 'package:dartz/dartz.dart';
import 'package:progiom_cms/src/features/core/core.dart';
import 'package:progiom_cms/src/features/core/util/util.dart';

import 'package:progiom_cms/src/features/ecommerce/domain/entities/OrderItem.dart';
import 'package:progiom_cms/src/features/ecommerce/domain/repositories/Ecommerce_repository.dart';

class GetOrderDetails extends UseCase<OrderItem, GetOrderDetailsParams> {
  final EcommerceRepository repository;
  GetOrderDetails(this.repository);

  @override
  Future<Either<Failure, OrderItem>> call(
      GetOrderDetailsParams params) async {
    return await repository.getOrderDetails(orderId: params.orderId);
  }
}

class GetOrderDetailsParams {
  final int orderId;

  GetOrderDetailsParams({
    required this.orderId,
  });
}

import 'package:dartz/dartz.dart';
import 'package:progiom_cms/src/features/core/core.dart';
import 'package:progiom_cms/src/features/core/util/util.dart';

import 'package:progiom_cms/src/features/ecommerce/domain/repositories/Ecommerce_repository.dart';
import 'package:progiom_cms/src/features/homeSettings/homeSettings.dart';

class AddToCart extends UseCase<bool, AddToCartParams> {
  final EcommerceRepository repository;
  AddToCart(this.repository);

  @override
  Future<Either<Failure, bool>> call(AddToCartParams params) async {
    final result = await repository.addToCart(
        productId: params.productId, qty: params.qty,selectedOptions:params.selectedOptions);
    CartNumber.instance.sync();

    return result;
  }
}

class AddToCartParams {
  final int productId;
  final int qty;
  final Map<String, String>? selectedOptions;
  AddToCartParams({
    required this.productId,
    required this.qty,
    this.selectedOptions
  });
}

import 'package:dartz/dartz.dart';
import 'package:progiom_cms/src/features/core/core.dart';
import 'package:progiom_cms/src/features/core/util/util.dart';
import 'package:progiom_cms/src/features/ecommerce/domain/entities/UseCouponResponse.dart';

import 'package:progiom_cms/src/features/ecommerce/domain/repositories/Ecommerce_repository.dart';

class UseCoupon extends UseCase<UseCouponResponse, UseCouponParams> {
  final EcommerceRepository repository;
  UseCoupon(this.repository);

  @override
  Future<Either<Failure, UseCouponResponse>> call(UseCouponParams params) async {
    final result = await repository.useCoupon(
      code: params.code,
    );
    return result;
  }
}

class UseCouponParams {
  final String code;
  

  UseCouponParams({
    required this.code,
  });
}

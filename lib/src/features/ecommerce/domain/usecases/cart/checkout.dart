import 'package:dartz/dartz.dart';
import 'package:progiom_cms/src/features/core/core.dart';
import 'package:progiom_cms/src/features/core/util/util.dart';
import 'package:progiom_cms/src/features/ecommerce/domain/entities/CheckoutModel.dart';

import 'package:progiom_cms/src/features/ecommerce/domain/repositories/Ecommerce_repository.dart';
import 'package:progiom_cms/src/features/homeSettings/homeSettings.dart';

class Checkout extends UseCase<bool, CheckoutParams> {
  final EcommerceRepository repository;
  Checkout(this.repository);

  @override
  Future<Either<Failure, bool>> call(CheckoutParams params) async {
    final result = await repository.checkout(
      params.checkoutModel,
    );
    CartNumber.instance.sync();
    return result;
  }
}

class CheckoutParams {
  final CheckoutModel checkoutModel;
  CheckoutParams({
    required this.checkoutModel,
  });
}

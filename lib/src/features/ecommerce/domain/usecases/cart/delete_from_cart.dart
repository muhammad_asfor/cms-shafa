import 'package:dartz/dartz.dart';
import 'package:progiom_cms/src/features/core/core.dart';
import 'package:progiom_cms/src/features/core/util/util.dart';

import 'package:progiom_cms/src/features/ecommerce/domain/repositories/Ecommerce_repository.dart';
import 'package:progiom_cms/src/features/homeSettings/domain/entities/cart_number_notifier.dart';

class DeleteFromCart extends UseCase<bool, DeleteFromCartParams> {
  final EcommerceRepository repository;
  DeleteFromCart(this.repository);

  @override
  Future<Either<Failure, bool>> call(DeleteFromCartParams params) async {
    CartNumber.instance.remove();
    final result = await repository.deleteFromCart(
      cartItemId: params.cartItemId,
    );

    return result;
  }
}

class DeleteFromCartParams {
  final int cartItemId;

  DeleteFromCartParams({
    required this.cartItemId,
  });
}

import 'package:dartz/dartz.dart';
import 'package:progiom_cms/src/features/core/core.dart';
import 'package:progiom_cms/src/features/core/util/util.dart';

import 'package:progiom_cms/src/features/ecommerce/domain/entities/my_points_response.dart';
import 'package:progiom_cms/src/features/ecommerce/domain/repositories/Ecommerce_repository.dart';

class GetMyPoints extends UseCase<MyPointsResponse, NoParams> {
  final EcommerceRepository repository;
  GetMyPoints(this.repository);

  @override
  Future<Either<Failure, MyPointsResponse>> call(
      NoParams noParams) async {
    return await repository.getMyPoints();
  }
}


import 'package:dartz/dartz.dart';
import 'package:progiom_cms/src/features/core/core.dart';
import 'package:progiom_cms/src/features/core/util/util.dart';
import 'package:progiom_cms/src/features/ecommerce/domain/entities/states_model.dart';

import 'package:progiom_cms/src/features/ecommerce/domain/repositories/Ecommerce_repository.dart';

class GetStates extends UseCase<List<StatesModel>, GetStatesParams> {
  final EcommerceRepository repository;
  GetStates(this.repository);

  @override
  Future<Either<Failure, List<StatesModel>>> call(
      GetStatesParams params) async {
    return await repository.getStates(params.countryId);
  }
}

class GetStatesParams {
  final int countryId;
  GetStatesParams(
    this.countryId,
  );
}

import 'package:dartz/dartz.dart';
import 'package:progiom_cms/src/features/core/core.dart';
import 'package:progiom_cms/src/features/core/util/util.dart';
import 'package:progiom_cms/src/features/ecommerce/domain/entities/CountryModel.dart';

import 'package:progiom_cms/src/features/ecommerce/domain/repositories/Ecommerce_repository.dart';

class GetCountries extends UseCase<List<CountryModel>, NoParams> {
  final EcommerceRepository repository;
  GetCountries(this.repository);

  @override
  Future<Either<Failure, List<CountryModel>>> call(NoParams noParams) async {
    return await repository.getCountries();
  }
}

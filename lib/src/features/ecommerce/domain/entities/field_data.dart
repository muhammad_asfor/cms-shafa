import 'package:json_annotation/json_annotation.dart';

part 'field_data.g.dart';

@JsonSerializable(fieldRename: FieldRename.snake)
class FieldData {
  final int id;
  final String? title;
  final String? value;
  // @CustomListConverter()
  final dynamic values;
  final String type;

  FieldData(this.id, this.title,
      this.values, this.type, this.value);

  factory FieldData.fromJson(json) => _$FieldDataFromJson(json);

  toJson() => _$FieldDataToJson(this);

  static List<FieldData> fromJsonList(List json) {
    return json.map((e) => FieldData.fromJson(e)).toList();
  }
}

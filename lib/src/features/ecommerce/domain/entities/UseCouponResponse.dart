import 'package:json_annotation/json_annotation.dart';

part 'UseCouponResponse.g.dart';

@JsonSerializable(fieldRename: FieldRename.snake)
class UseCouponResponse {
  final String newPrice;

  UseCouponResponse({
    required this.newPrice,
  });

  factory UseCouponResponse.fromJson(json) => _$UseCouponResponseFromJson(json);
  toJson() => _$UseCouponResponseToJson(this);

  static List<UseCouponResponse> fromJsonList(List json) {
    return json.map((e) => UseCouponResponse.fromJson(e)).toList();
  }
}

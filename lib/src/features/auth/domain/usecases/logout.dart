import 'package:progiom_cms/src/features/auth/domain/repositories/auth_repository.dart';

import 'package:progiom_cms/src/features/core/util/util.dart';
import 'package:dartz/dartz.dart';

class Logout extends UseCase<void, NoParams> {
  final AuthRepository repository;

  Logout(
    this.repository,
  );
  @override
  Future<Either<Failure, void>> call(NoParams params) async {
    
    return await repository.logout();
  }
}

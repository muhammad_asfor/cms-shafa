import 'package:progiom_cms/src/features/auth/domain/repositories/auth_repository.dart';
import 'package:progiom_cms/src/features/core/SharedEntities/User.dart';

import 'package:progiom_cms/src/features/core/util/util.dart';

import 'package:dartz/dartz.dart';

import '../../settings.dart';

class SocialLogin extends UseCase<User, SocialLoginParams> {
  final AuthRepository repository;

  SocialLogin(
    this.repository,
  );
  @override
  Future<Either<Failure, User>> call(SocialLoginParams params) async {
    return await repository.socialLogin(
        provider: params.provider == SocialLoginProvider.face
            ? "facebook?token="
            : params.provider == SocialLoginProvider.google
                ? "google?code="
                : "sign-in-with-apple?token=",
        token: params.token);
  }
}

class SocialLoginParams {
  final String token;
  final SocialLoginProvider provider;

  SocialLoginParams({
    required this.provider,
    required this.token,
  });
}

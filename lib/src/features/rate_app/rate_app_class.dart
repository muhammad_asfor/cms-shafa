import 'package:flutter/material.dart';
import 'package:progiom_cms/src/features/rate_app/presentation/widgets/rate_app_widget.dart';

import '../../../Getit_instance.dart';
import '../../../core.dart';
import 'domain/usecases/init_rate_app.dart';

class RateApp {
  Future<void> checkRateApp(
    BuildContext context,
  ) async {
    final result = await sl<InitRateTime>().call(NoParams());
    result.fold((l) {
      print(l.errorMessage);
    }, (isShowDialog) {
      if (isShowDialog) {
        //  showRateDialoge(context, sizeConfig)
      } else {}
    });
  }

  showRateWidget(context, sizeConfig) {
    showRateDialoge(context, sizeConfig);
  }
}

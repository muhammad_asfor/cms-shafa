import 'package:dartz/dartz.dart';
import '../../../../../core.dart';
import '../repositories/rateApp_repository.dart';

class InitRateTime extends UseCase<bool, NoParams> {
  final RateAppRepository repository;
  InitRateTime(this.repository);

  @override
  Future<Either<Failure, bool>> call(NoParams noParams) async {
    final result = await repository.initRateTime();
    return result.fold((l) {
      return Left(CacheFailure(l.errorMessage));
    }, (isAlreadyRegistered) async {
      //- 0 mean already showed
      //- null not registered yet
      //- date
      if (isAlreadyRegistered != null && isAlreadyRegistered != "0") {
        final currentTime = DateTime.now();
        final registeredTime = DateTime.parse(isAlreadyRegistered);
        if (currentTime.difference(registeredTime).inDays >= 3) {
          return Right(true);
        }

        return Right(false);
      } else {
        await repository.register();
        return Right(false);
      }
    });
  }
}

import 'package:progiom_cms/Getit_instance.dart';
import 'package:progiom_cms/src/features/homeSettings/data/datasources/HomeSettingsApi.dart';
import 'package:progiom_cms/src/features/homeSettings/data/datasources/home_settings_local_data_source.dart';
import 'package:progiom_cms/src/features/homeSettings/data/repositories/settings_repo_impl.dart';
import 'package:progiom_cms/src/features/homeSettings/domain/repositories/settings_repository.dart';

void initSettingsInjections() async {
  sl.registerLazySingleton<HomeSettingsApi>(() => HomeSettingsApi());
  sl.registerLazySingleton<HomeSettingsLocalDataSource>(
      () => HomeSettingsLocalDataSource(sl()));
  sl.registerLazySingleton<SettingsRepository>(() => SettingsRepositoryImpl(
        sl(),
        sl(),
      ));
}

import 'package:json_annotation/json_annotation.dart';
import 'package:progiom_cms/src/features/core/SharedEntities/BaseListResponse.dart';
import 'package:progiom_cms/src/features/core/SharedEntities/Product.dart';
import 'package:progiom_cms/src/features/homeSettings/domain/entities/doctor_card.dart';
import 'package:progiom_cms/src/features/homeSettings/domain/entities/reviews.dart';
import 'package:progiom_cms/src/features/homeSettings/domain/entities/speciality_doctor.dart';

part 'TopDoctor.g.dart';

@JsonSerializable(fieldRename: FieldRename.snake)
class TopDoctor {
  final int id;
  final String name;
  final String coverImage;
  final SpecialityDoctor? speciality;
  final DoctorCard? doctor_card;
  final double user_rating;

  TopDoctor(
      {required this.id,
      this.speciality,
      required this.name,
      this.doctor_card,
      required this.user_rating,
      required this.coverImage});

  factory TopDoctor.fromJson(json) => _$TopDoctorFromJson(json);
  toJson() => _$TopDoctorToJson(this);

  static List<TopDoctor> fromJsonList(List json) {
    return json.map((e) => TopDoctor.fromJson(e)).toList();
  }
}

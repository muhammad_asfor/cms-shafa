// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'reviews.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

reviews _$reviewsFromJson(Map<String, dynamic> json) => reviews(
      id: json['id'] as int,
      user_id: json['user_id'] as int,
      rate: json['rate'] as int,
      comment: json['comment'] as String?,
      created_at: json['created_at'] as String?,
      title: json['title'] as String?,
    );

Map<String, dynamic> _$reviewsToJson(reviews instance) => <String, dynamic>{
      'id': instance.id,
      'user_id': instance.user_id,
      'rate': instance.rate,
      'comment': instance.comment,
      'created_at': instance.created_at,
      'title': instance.title,
    };

// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'speciality_doctor.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

SpecialityDoctor _$SpecialityDoctorFromJson(Map<String, dynamic> json) =>
    SpecialityDoctor(
      title: json['title'] as String,
    );

Map<String, dynamic> _$SpecialityDoctorToJson(SpecialityDoctor instance) =>
    <String, dynamic>{
      'title': instance.title,
    };

// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'TopDoctor.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

TopDoctor _$TopDoctorFromJson(Map<String, dynamic> json) => TopDoctor(
      id: json['id'] as int,
      speciality: json['speciality'] == null
          ? null
          : SpecialityDoctor.fromJson(json['speciality']),
      name: json['name'] as String,
      doctor_card: json['doctor_card'] == null
          ? null
          : DoctorCard.fromJson(json['doctor_card']),
      user_rating: (json['user_rating'] as num).toDouble(),
      coverImage: json['cover_image'] as String,
    );

Map<String, dynamic> _$TopDoctorToJson(TopDoctor instance) => <String, dynamic>{
      'id': instance.id,
      'name': instance.name,
      'cover_image': instance.coverImage,
      'speciality': instance.speciality,
      'doctor_card': instance.doctor_card,
      'user_rating': instance.user_rating,
    };

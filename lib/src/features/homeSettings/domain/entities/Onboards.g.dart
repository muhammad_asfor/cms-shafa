// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'Onboards.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

Onboards _$OnboardsFromJson(Map<String, dynamic> json) => Onboards(
      id: json['id'] as int,
      coverImage: json['cover_image'] as String?,
      title: json['title'] as String?,
      description: json['description'] as String?,
    );

Map<String, dynamic> _$OnboardsToJson(Onboards instance) => <String, dynamic>{
      'id': instance.id,
      'cover_image': instance.coverImage,
      'title': instance.title,
      'description': instance.description,
    };

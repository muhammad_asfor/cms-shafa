import 'package:json_annotation/json_annotation.dart';
import 'package:progiom_cms/src/features/core/SharedEntities/BaseListResponse.dart';
import 'package:progiom_cms/src/features/core/SharedEntities/Product.dart';
import 'package:progiom_cms/src/features/homeSettings/domain/entities/doctor_card.dart';
import 'package:progiom_cms/src/features/homeSettings/domain/entities/reviews.dart';
import 'package:progiom_cms/src/features/homeSettings/domain/entities/speciality_doctor.dart';

part 'TopDoctorWithReviews.g.dart';

@JsonSerializable(fieldRename: FieldRename.snake)
class TopDoctorWithReviews {
  final int id;
  final String name;
  final String coverImage;
  final SpecialityDoctor? speciality;
  final DoctorCard? doctor_card;
  final double user_rating;
  final int clients_count;
  final int certificates_count;
  final String? country_text;
  final String? state_text;
  final String? city_text;
  final String? address;

  final List<reviews> latest_reviews;
  TopDoctorWithReviews({
    required this.id,
    this.speciality,
    required this.name,
    required this.latest_reviews,
    required this.clients_count,
    this.doctor_card,
    required this.certificates_count,
    required this.user_rating,
    required this.coverImage,
    this.address,
    this.city_text,
    this.country_text,
    this.state_text,
  });

  factory TopDoctorWithReviews.fromJson(json) =>
      _$TopDoctorWithReviewsFromJson(json);
  toJson() => _$TopDoctorWithReviewsToJson(this);

  static List<TopDoctorWithReviews> fromJsonList(List json) {
    return json.map((e) => TopDoctorWithReviews.fromJson(e)).toList();
  }
}

import 'package:json_annotation/json_annotation.dart';
import 'package:progiom_cms/src/features/core/SharedEntities/BaseListResponse.dart';
import 'package:progiom_cms/src/features/core/SharedEntities/Product.dart';

part 'speciality_doctor.g.dart';

@JsonSerializable(fieldRename: FieldRename.snake)
class SpecialityDoctor {
  final String title;

  SpecialityDoctor({
    required this.title,
  });

  factory SpecialityDoctor.fromJson(json) => _$SpecialityDoctorFromJson(json);
  toJson() => _$SpecialityDoctorToJson(this);
}

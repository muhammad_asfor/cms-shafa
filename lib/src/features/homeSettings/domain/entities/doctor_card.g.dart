// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'doctor_card.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

DoctorCard _$DoctorCardFromJson(Map<String, dynamic> json) => DoctorCard(
      start_work_time: json['start_work_time'] as String?,
      end_work_time: json['end_work_time'] as String?,
      examination_period: json['examination_period'] as int?,
      bio: json['bio'] as String?,
      experience: json['experience'] as String?,
      experience_years: json['experience_years'] as int?,
      address: json['address'] as String?,
      certificates_bag: (json['certificates_bag'] as List<dynamic>?)
          ?.map((e) => e as String)
          .toList(),
    );

Map<String, dynamic> _$DoctorCardToJson(DoctorCard instance) =>
    <String, dynamic>{
      'start_work_time': instance.start_work_time,
      'end_work_time': instance.end_work_time,
      'bio': instance.bio,
      'experience': instance.experience,
      'experience_years': instance.experience_years,
      'examination_period': instance.examination_period,
      'address': instance.address,
      'certificates_bag': instance.certificates_bag,
    };

import 'package:json_annotation/json_annotation.dart';
import 'package:progiom_cms/src/features/homeSettings/domain/entities/TopDoctor.dart';
part 'slide.g.dart';

@JsonSerializable(fieldRename: FieldRename.snake)
class Slide {
  final int id;
  final String? date;
  final String? time;
  final TopDoctor? doctor;

  factory Slide.fromJson(json) => _$SlideFromJson(json);
  toJson() => _$SlideToJson(this);
  static List<Slide> fromJsonList(List json) {
    return json.map((e) => Slide.fromJson(e)).toList();
  }

  Slide({required this.id, this.date, this.time, this.doctor});
}

// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'TopDoctorWithReviews.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

TopDoctorWithReviews _$TopDoctorWithReviewsFromJson(
        Map<String, dynamic> json) =>
    TopDoctorWithReviews(
      id: json['id'] as int,
      speciality: json['speciality'] == null
          ? null
          : SpecialityDoctor.fromJson(json['speciality']),
      name: json['name'] as String,
      latest_reviews: (json['latest_reviews'] as List<dynamic>)
          .map((e) => reviews.fromJson(e))
          .toList(),
      clients_count: json['clients_count'] as int,
      doctor_card: json['doctor_card'] == null
          ? null
          : DoctorCard.fromJson(json['doctor_card']),
      certificates_count: json['certificates_count'] as int,
      user_rating: (json['user_rating'] as num).toDouble(),
      coverImage: json['cover_image'] as String,
      address: json['address'] as String?,
      city_text: json['city_text'] as String?,
      country_text: json['country_text'] as String?,
      state_text: json['state_text'] as String?,
    );

Map<String, dynamic> _$TopDoctorWithReviewsToJson(
        TopDoctorWithReviews instance) =>
    <String, dynamic>{
      'id': instance.id,
      'name': instance.name,
      'cover_image': instance.coverImage,
      'speciality': instance.speciality,
      'doctor_card': instance.doctor_card,
      'user_rating': instance.user_rating,
      'clients_count': instance.clients_count,
      'certificates_count': instance.certificates_count,
      'country_text': instance.country_text,
      'state_text': instance.state_text,
      'city_text': instance.city_text,
      'address': instance.address,
      'latest_reviews': instance.latest_reviews,
    };

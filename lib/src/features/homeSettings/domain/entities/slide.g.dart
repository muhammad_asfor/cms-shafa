// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'slide.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

Slide _$SlideFromJson(Map<String, dynamic> json) => Slide(
      id: json['id'] as int,
      date: json['date'] as String?,
      time: json['time'] as String?,
      doctor:
          json['doctor'] == null ? null : TopDoctor.fromJson(json['doctor']),
    );

Map<String, dynamic> _$SlideToJson(Slide instance) => <String, dynamic>{
      'id': instance.id,
      'date': instance.date,
      'time': instance.time,
      'doctor': instance.doctor,
    };

import 'package:json_annotation/json_annotation.dart';
import 'package:progiom_cms/src/features/core/SharedEntities/BaseListResponse.dart';
import 'package:progiom_cms/src/features/core/SharedEntities/Product.dart';

part 'reviews.g.dart';

@JsonSerializable(fieldRename: FieldRename.snake)
class reviews {
  final int id;
  final int user_id;
  final int rate;
  final String? comment;
  final String? created_at;
  final String? title;

  reviews(
      {required this.id,
      required this.user_id,
      required this.rate,
      this.comment,
      this.created_at,
      this.title});

  factory reviews.fromJson(json) => _$reviewsFromJson(json);
  toJson() => _$reviewsToJson(this);
}

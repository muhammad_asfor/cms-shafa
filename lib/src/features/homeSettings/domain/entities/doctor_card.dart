import 'package:json_annotation/json_annotation.dart';
import 'package:progiom_cms/src/features/core/SharedEntities/BaseListResponse.dart';
import 'package:progiom_cms/src/features/core/SharedEntities/Product.dart';

part 'doctor_card.g.dart';

@JsonSerializable(fieldRename: FieldRename.snake)
class DoctorCard {
  String? start_work_time;
  String? end_work_time;
  final String? bio;
  final String? experience;
  final int? experience_years;
  final int? examination_period;
  final String? address;
  final List<String>? certificates_bag;

  DoctorCard(
      {this.start_work_time,
      this.end_work_time,
      this.examination_period,
      this.bio,
      this.experience,
      this.experience_years,
      this.address,
      this.certificates_bag});

  factory DoctorCard.fromJson(json) => _$DoctorCardFromJson(json);
  toJson() => _$DoctorCardToJson(this);
}

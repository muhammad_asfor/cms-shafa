import 'package:dartz/dartz.dart';

import 'package:progiom_cms/src/features/core/util/util.dart';
import 'package:progiom_cms/src/features/homeSettings/domain/repositories/settings_repository.dart';

class GetPreferences extends UseCase<Map, NoParams> {
  final SettingsRepository repository;

  GetPreferences(
    this.repository,
  );
  @override
  Future<Either<Failure, Map>> call(NoParams noParams) async {
    return await repository.getPreferences();
  }
}

import 'package:dartz/dartz.dart';
import 'package:progiom_cms/core.dart';

import 'package:progiom_cms/src/features/homeSettings/domain/repositories/settings_repository.dart';

import '../../homeSettings.dart';

class GetCategories extends UseCase<List<Specialitie>, NoParams> {
  final SettingsRepository repository;

  GetCategories(
    this.repository,
  );

  @override
  Future<Either<Failure, List<Specialitie>>> call(NoParams params) async {
    return await repository.getCategories();
  }
}

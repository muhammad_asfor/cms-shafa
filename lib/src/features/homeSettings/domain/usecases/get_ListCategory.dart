import 'package:dartz/dartz.dart';
import 'package:progiom_cms/core.dart';
import 'package:progiom_cms/src/features/homeSettings/data/datasources/HomeSettingsApi.dart';

import 'package:progiom_cms/src/features/homeSettings/domain/repositories/settings_repository.dart';

import '../../homeSettings.dart';

class GetListCategories extends UseCase<List<Category>, SearchCategoryParams> {
  final SettingsRepository repository;

  GetListCategories(
    this.repository,
  );

  @override
  Future<Either<Failure, List<Category>>> call(
      SearchCategoryParams params) async {
    return await repository.getLsitCategories(params);
  }
}
